resource "digitalocean_droplet" "server" {
    image = "debian-10-x64"
    name = "server"
    region = "sfo2"
    size = "s-1vcpu-1gb"
    monitoring = "true"
    ipv6 = "true"
    tags = ["${digitalocean_tag.game.id}"]
}

