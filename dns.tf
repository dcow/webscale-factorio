resource "digitalocean_domain" "default" {
   name = "factorio.city"
}

resource "digitalocean_record" "spyc-A" {
  domain = "${digitalocean_domain.default.name}"
  type   = "A"
  name   = "spyc"
  value = "${digitalocean_droplet.server.ipv4_address}"
}

resource "digitalocean_record" "spyc-AAAA" {
  domain = "${digitalocean_domain.default.name}"
  type   = "AAAA"
  name   = "spyc"
  value = "${digitalocean_droplet.server.ipv6_address}"
}

resource "digitalocean_record" "temp-A" {
  domain = "${digitalocean_domain.default.name}"
  type   = "A"
  name   = "temp"
  value = "167.71.153.171"
}

resource "digitalocean_record" "temp-AAAA" {
  domain = "${digitalocean_domain.default.name}"
  type   = "AAAA"
  name   = "temp"
  value = "2604:a880:2:d1::b4:e001"
}
