resource "digitalocean_project" "factorio" {
    name        = "Factorio"
    description = "A web scale factorio multiplayer server."
    purpose     = "Game Multiplayer"
    environment = "Production"
    resources = [
        "${digitalocean_droplet.server.urn}",
        "${digitalocean_domain.default.urn}",
    ]
}

